# qbound shared demo components

 <img src="https://www.qbound.io/wp-content/uploads/2020/04/qbound_logo@288x288.png" alt="logo" width="800">

> Please find below the prepared and usable demo components that will allow you to setup and manage your own applications as well as devices via the qbound policy engine. If you require further information please don't hesitate to get in touch with us: contact@qbound.io

# Components

The overall qbound architecture includes four pre-configured software components.

- **Policy Engine:** Your cloud-based authentication and authorization instance. Here you are able to manage all users, user rights, clients, devices, gateways and application. This is place where you have full visibility and control of what is going on in your network.
- **Client:** Your client to initiate the authentication and authorization, and allowing you to access any of the network components (e.g. policy engine, application).
- **Gateway:** Your individual gateways allow you to establish individual data connections between the client and the "target" application/device. Here your individual filter criteria and rules are enforced to analyse the data transfer in real time.
- **Connector:** Secures your application/device and only establishes the outbound connection to the Gateway. No acceptance of incoming and reactions to external requests.

The - after setup - orchestration of the aformentioned component is illustrated in the following figure.

 <img src="FiguresReadme/qbound_overview_solution.png" alt="Overview" width="800">

# Setup

> The initial installation (onboarding) process includes four main steps and can be done in only a few minutes.

#### Step 1: Download and start the qbound Client (initial credentials will be shared with you)

- Start qbound.exe (depending on your installation path - C:\Program Files\qbound\qbound.exe)
- Login with your credentials (recieved via email)
- Connect to your secure network

Proccess should look like that:

 <img src="FiguresReadme/Client.PNG" alt="Client" width="250"> 
 
 <img src="FiguresReadme/ConnectClient.PNG" alt="ConnectClient" width="250">

 <img src="FiguresReadme/ConnectedClient.PNG" alt="ConnectedClient" width="250">

#### Step 2: Login to the qbound Policy Engine

- Only in case your qbound client is connected: You are now able to access the qbound Policy Engine (http://demo.qbound.io/)
- Login with your credentials - single sign-on possible (recieved via email)

Proccess should look like that:

 <img src="FiguresReadme/PolicyEngineLogin.PNG" alt="PolicyEngineLogin" width="500"> 
 <img src="FiguresReadme/Dashboard.PNG" alt="Dashboard" width="800">

#### Step 3: Add and configure your application/device (that you want to secure) within the Policy Engine

- Navigate: Manage -> Applications -> Create Application
- Fill the Details

* [ ] Name: free text
* [ ] Description: free text
* [ ] URL: needs to start with protocol "http or https"
* [ ] Image URL: any image URL
* [ ] Port: service port of application
* [ ] Public IP Address: only if applicable
* [ ] Protocol: http or https
* [ ] Type: web

Proccess should look like that:

 <img src="FiguresReadme/AddApplication.PNG" alt="AddApplication" width="800"> 
 <img src="FiguresReadme/AppOverview.PNG" alt="AppOverview" width="800">

#### Step 4: Install the qbound Connector on your application/device

Download and run the installer file to install the connector on the host you want to provide secure access.
Currently there are two installer scripts. One for the x86 and one for armv7 architecture.

Be sure to swap out \${ARCHITECTURE} with either x86 or armv7 in the steps belog.

```sh
$ wget -O install.sh https://gitlab.com/qboundio/shared/-/raw/master/scripts/install_${ARCHITECTURE}.sh
$ chmod +x install.sh
$ ./install.sh
```

After installing the required dependencies you will have to run the enrollment with the enrollment key provided to you after adding an application to the policy engine via the frontend.

```sh
$ ./connector enroll --key="${ENROLLMENT_KEY}" --config="./config-client.yaml"
```

This will enroll the client with the Policy Engine and store a configuration that allows for outbound connections to the qbound gateways. Be sure to swap out \${ENROLLMENT_KEY} with the actual enrollment key.

The enrollment key can be found here: Manage -> Applications
<br>
<img src="FiguresReadme/CopyEnrollmentKey.PNG" alt="Key" width="500">

After successful enrollment connect the device

```sh
$ ./connector connect
```

A connected device can be disconnected using the following command:

```sh
$ ./connector disconnect
```

#### --- Initial installation (onboarding) process done!


<br>
<br>

# Request and approve/deny access


#### 1: Get overview of currently allowed access

- Navigate: Visualization

The diagram will display the current configuration and show you "green" connections for currently allow access (here none)

 <img src="FiguresReadme/AccessOverview.png" alt="accessVisualization" width="800">
 
 
 In addition you can get an overview via: 
 - Navigate: Access Requests -> Open Access Requests 

<img src="FiguresReadme/AccessRequests.png" alt="accessRequests" width="800">
 
 
 #### 2: Create new access request
 
- Navigate: Access Requests -> Open Access Requests -> Create Access Requests
- Fill in the details

<img src="FiguresReadme/AccesRequestAdd.png" alt="createAccessRequests" width="800">
 
 
 #### 3: Approve or deny access request - Please note: In the demo environment you can approve and deny also your own request
 
- Navigate: Access Requests -> Open Access Requests -> select the open request you want to answer
- Navigate: More -> Approve selected / Deny selected


<img src="FiguresReadme/AccessRequestAction.png" alt="respondAccessRequests" width="800">
 
 

