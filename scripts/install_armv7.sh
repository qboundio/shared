#!/bin/bash

sudo apt update && sudo apt upgrade -y
sudo apt-get install raspberrypi-kernel-headers
echo "deb http://deb.debian.org/debian/ unstable main" | sudo tee --append /etc/apt/sources.list.d/unstable.list
wget -O - https://ftp-master.debian.org/keys/archive-key-$(lsb_release -sr).asc | sudo apt-key add -
printf 'Package: *\nPin: release a=unstable\nPin-Priority: 150\n' | sudo tee --append /etc/apt/preferences.d/limit-unstable
sudo apt update
sudo apt install wireguard -y

wget -O shared.tar.gz  https://gitlab.com/qboundio/shared/-/archive/master/shared-master.tar.gz?path=connector-linux
tar -xf shared.tar.gz
rm shared.tar.gz && mv shared-master-connector-linux/connector-linux . && rm -rf shared-master-connector-linux
cd connector-linux && mkdir conf && chmod +x connector-armv7 && chmod +x connector-x86


